import socios_model

cuota_socio = 100.0
descuento_menores = 0.5
descuento_familia_numerosa6 = 0.2
descuento_familia_numerosa10 = 0.3

class Socio:
    def __init__(self, id_socio, nombre_socio, apellido_socio, DNI_socio, cant_grupo_familiar, cant_menores18):
        self.id_socio = int(id_socio)
        self.apellido_socio = apellido_socio
        self.nombre_socio = nombre_socio
        self.DNI_socio = DNI_socio
        self.cant_grupo_familiar = int(cant_grupo_familiar)
        self.cant_menores18 = int(cant_menores18)


    def obtener_socio_x_Id(self):
        socio_encontrado = socios_model.buscar_socio_x_Id(self.id_socio)
        socio_devuelto = Socio(socio_encontrado[0], socio_encontrado[1], socio_encontrado[2], socio_encontrado[3], socio_encontrado[4], 
                                socio_encontrado[5])
        return socio_devuelto

    def guardar_socio(self):
        socio_nuevo = socios_model.alta_socio((self.id_socio, self.nombre_socio, self.apellido_socio, self.DNI_socio, 
                                        self.cant_grupo_familiar, self.cant_menores18))
        socio_guardado = Socio(socio_nuevo[0], socio_nuevo[1], socio_nuevo[2], socio_nuevo[3], socio_nuevo[4], 
                                socio_nuevo[5])
        return socio_guardado

    def eliminar_socio(self):
        print(self.id_socio)
        return socios_model.baja_socio(self.id_socio)

    def modificar_socio(self):
        socio_modificado = (self.id_socio, self.nombre_socio, self.apellido_socio, self.DNI_socio, 
                                        self.cant_grupo_familiar, self.cant_menores18)
        # print(socio_modificado)
        return socios_model.modificacion_socio(socio_modificado)

    def calcular_cuota(self):
        cuota_grupo_familiar = cuota_socio * (self.cant_grupo_familiar-self.cant_menores18) +\
                                cuota_socio * (1-descuento_menores) * self.cant_menores18
        if self.cant_grupo_familiar>=10:
            return cuota_grupo_familiar * (1 - descuento_familia_numerosa10)
        elif self.cant_grupo_familiar>=6:
            return cuota_grupo_familiar * (1 - descuento_familia_numerosa6)
        else:
            return cuota_grupo_familiar
