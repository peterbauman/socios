import pymysql
from pymysql.err import Error

#   TABLE `socios` 
#   (
#   `id_socio` int(11) NOT NULL,
#   `nombres_socio` text NOT NULL,
#   `apellidos_socio` text NOT NULL,
#   `dni_socio` bigint(20) NOT NULL,
#   `cant_grupo_familiar` int(11) NOT NULL DEFAULT 0,
#   `cant_menores18` int(11) NOT NULL DEFAULT 0
#   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


def abrir_conexion():
    try: 
        conexion = pymysql.connect(host='localhost',
                                    user='root',
                                    password='',
                                    db='abm_socios')
        print("La conexión a la base de datos fue correcta")
        return conexion
    except (Exception, Error) as error_capturado:
        print("Ocurrió el siguiente error en la conexión a la base de datos: ", error_capturado)
    

def cerrar_conexion(conexion):
    try: 
        conexion.close()
        print("La conexión a la base de datos cerrada de forma correcta")
    except (Exception, Error) as error_capturado:
        print("Ocurrió el siguiente error al cerrar la conexión a la base de datos: ", error_capturado)


def buscar_socio_x_Id(id_buscar):
    try:
        conexion = abrir_conexion()
        cursor =  conexion.cursor()
        query = 'SELECT id_socio, nombres_socio, apellidos_socio, dni_socio, cant_grupo_familiar, cant_menores18 \
                FROM socios WHERE id_socio = %s;'
        cursor.execute(query, id_buscar)
        socio = cursor.fetchone()
        cerrar_conexion(conexion)
        return socio
    except:
        return False


def modificacion_socio(socio):
    try:
        conexion = abrir_conexion()
        cursor =  conexion.cursor()
        query = 'UPDATE socios SET nombres_socio = %s, apellidos_socio = %s, dni_socio = %s, cant_grupo_familiar = %s,\
                cant_menores18 = %s WHERE id_socio = %s;'
        values = (socio[1], socio[2], socio[3], socio[4], socio[5], socio[0])
        cursor.execute(query, values)
        conexion.commit()
        cerrar_conexion(conexion)
        return True
    except:
        return False


def baja_socio(id_eliminar):
    try:
        conexion = abrir_conexion()
        cursor =  conexion.cursor()
        query = 'DELETE FROM socios WHERE id_socio = %s;'
        cursor.execute(query, id_eliminar)
        conexion.commit()
        cerrar_conexion(conexion)
        return True
    except:
        return False

def alta_socio(socio):
    try:
        conexion = abrir_conexion()
        cursor =  conexion.cursor()
        query = 'INSERT INTO socios(nombres_socio, apellidos_socio, dni_socio, cant_grupo_familiar, cant_menores18) VALUES (%s, %s, %s, %s, %s);'
        cursor.execute(query, socio[1:])
        conexion.commit()
        query = 'SELECT * FROM socios WHERE id_socio = (SELECT MAX(id_socio) FROM socios)'
        cursor.execute(query)
        socio = cursor.fetchone()
        cerrar_conexion(conexion) 
        return socio
    except:
        return False

# alta_socio([1, 'Steve', 'Rogers', '26505640', 0, 0])
# alta_socio([1, 'Clark', 'Kent', '26505641', 4, 2])
# alta_socio([1, 'Peter', 'Parker', '26505642', 2, 1])
# alta_socio([1, 'Bruce', 'Wayne', '26505643', 3, 0])

# print(buscar_socio_x_Id(1))

# modificacion_socio([3, 'Peter', 'Parker', '45505642', 2, 1])
# print(buscar_socio_x_Id(3))

# alta_socio([1, 'Bruce', 'Wayne', '26505643', 3, 0])
# print(buscar_socio_x_Id(5))

# baja_socio(5)